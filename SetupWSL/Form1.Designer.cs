﻿
namespace SetupWSL
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(string btnName, string label2Text)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.button = new SetupWSL.Form1.RoundButton();
            this.label1 = new System.Windows.Forms.Label();
            this.lblOutput = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.step7 = new System.Windows.Forms.Label();
            this.step6 = new System.Windows.Forms.Label();
            this.step5 = new System.Windows.Forms.Label();
            this.step4 = new System.Windows.Forms.Label();
            this.step3 = new System.Windows.Forms.Label();
            this.step2 = new System.Windows.Forms.Label();
            this.step1 = new System.Windows.Forms.Label();
            this.check7 = new System.Windows.Forms.Label();
            this.check6 = new System.Windows.Forms.Label();
            this.check5 = new System.Windows.Forms.Label();
            this.check4 = new System.Windows.Forms.Label();
            this.check3 = new System.Windows.Forms.Label();
            this.check2 = new System.Windows.Forms.Label();
            this.check1 = new System.Windows.Forms.Label();
            this.spinner = new System.Windows.Forms.PictureBox();
            this.preInstallationPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnInstall = new SetupWSL.Form1.RoundButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinner)).BeginInit();
            this.preInstallationPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(780, 425);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(202, 93);
            this.txtOutput.TabIndex = 0;
            this.txtOutput.Visible = false;

            // 
            // button
            // 
            this.button.BackColor = System.Drawing.Color.White;
            this.button.FlatAppearance.BorderSize = 0;
            this.button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button.ForeColor = System.Drawing.Color.Black;
            this.button.Location = new System.Drawing.Point(427, 295);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(224, 35);
            this.button.TabIndex = 1;
            this.button.Text = "CANCELAR";
            this.button.UseVisualStyleBackColor = false;
            this.button.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Raleway", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(293, 151);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(500, 40);
            this.label1.TabIndex = 2;
            this.label1.Text = "Instalación de Ubuntu";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOutput
            // 
            this.lblOutput.Font = new System.Drawing.Font("Raleway", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutput.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblOutput.Location = new System.Drawing.Point(293, 200);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(500, 20);
            this.lblOutput.TabIndex = 2;
            this.lblOutput.Text = "Habilitando Plataforma de Maquina Virtual";
            this.lblOutput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(499, 60);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(76, 70);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(392, 224);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(300, 23);
            this.progressBar.TabIndex = 4;
            this.progressBar.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(32)))), ((int)(((byte)(47)))));
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.step7);
            this.panel1.Controls.Add(this.step6);
            this.panel1.Controls.Add(this.step5);
            this.panel1.Controls.Add(this.step4);
            this.panel1.Controls.Add(this.step3);
            this.panel1.Controls.Add(this.step2);
            this.panel1.Controls.Add(this.step1);
            this.panel1.Controls.Add(this.check7);
            this.panel1.Controls.Add(this.check6);
            this.panel1.Controls.Add(this.check5);
            this.panel1.Controls.Add(this.check4);
            this.panel1.Controls.Add(this.check3);
            this.panel1.Controls.Add(this.check2);
            this.panel1.Controls.Add(this.check1);
            this.panel1.Controls.Add(this.spinner);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(275, 366);
            this.panel1.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Raleway", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gainsboro;
            this.label4.Location = new System.Drawing.Point(51, 273);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 13;
            this.label4.Text = "Desarrollado por";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(45, 296);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(145, 46);
            this.pictureBox4.TabIndex = 12;
            this.pictureBox4.TabStop = false;
            // 
            // step7
            // 
            this.step7.AutoSize = true;
            this.step7.Font = new System.Drawing.Font("Raleway SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.step7.ForeColor = System.Drawing.Color.Gray;
            this.step7.Location = new System.Drawing.Point(42, 212);
            this.step7.Name = "step7";
            this.step7.Size = new System.Drawing.Size(83, 16);
            this.step7.TabIndex = 1;
            this.step7.Text = "Desinstalando";
            // 
            // step6
            // 
            this.step6.AutoSize = true;
            this.step6.Font = new System.Drawing.Font("Raleway SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.step6.ForeColor = System.Drawing.Color.Gray;
            this.step6.Location = new System.Drawing.Point(42, 184);
            this.step6.Name = "step6";
            this.step6.Size = new System.Drawing.Size(117, 16);
            this.step6.TabIndex = 1;
            this.step6.Text = "Instalación finalizada";
            // 
            // step5
            // 
            this.step5.AutoSize = true;
            this.step5.Font = new System.Drawing.Font("Raleway SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.step5.ForeColor = System.Drawing.Color.Gray;
            this.step5.Location = new System.Drawing.Point(42, 156);
            this.step5.Name = "step5";
            this.step5.Size = new System.Drawing.Size(203, 16);
            this.step5.TabIndex = 1;
            this.step5.Text = "Aplicando configuraciones de Ceibal";
            // 
            // step4
            // 
            this.step4.AutoSize = true;
            this.step4.Font = new System.Drawing.Font("Raleway SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.step4.ForeColor = System.Drawing.Color.Gray;
            this.step4.Location = new System.Drawing.Point(42, 128);
            this.step4.Name = "step4";
            this.step4.Size = new System.Drawing.Size(148, 16);
            this.step4.TabIndex = 1;
            this.step4.Text = "Instalando entorno gráfico";
            // 
            // step3
            // 
            this.step3.AutoSize = true;
            this.step3.Font = new System.Drawing.Font("Raleway SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.step3.ForeColor = System.Drawing.Color.Gray;
            this.step3.Location = new System.Drawing.Point(42, 100);
            this.step3.Name = "step3";
            this.step3.Size = new System.Drawing.Size(124, 16);
            this.step3.TabIndex = 1;
            this.step3.Text = "Configurando Ubuntu";
            // 
            // step2
            // 
            this.step2.AutoSize = true;
            this.step2.Font = new System.Drawing.Font("Raleway SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.step2.ForeColor = System.Drawing.Color.Gray;
            this.step2.Location = new System.Drawing.Point(42, 72);
            this.step2.Name = "step2";
            this.step2.Size = new System.Drawing.Size(172, 16);
            this.step2.TabIndex = 1;
            this.step2.Text = "Instalando distribución Ubuntu";
            // 
            // step1
            // 
            this.step1.AutoSize = true;
            this.step1.Font = new System.Drawing.Font("Raleway SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.step1.ForeColor = System.Drawing.Color.White;
            this.step1.Location = new System.Drawing.Point(42, 46);
            this.step1.Name = "step1";
            this.step1.Size = new System.Drawing.Size(152, 16);
            this.step1.TabIndex = 1;
            this.step1.Text = "Verificando requerimientos";
            // 
            // check7
            // 
            this.check7.AutoSize = true;
            this.check7.ForeColor = System.Drawing.Color.Lime;
            this.check7.Location = new System.Drawing.Point(17, 212);
            this.check7.Name = "check7";
            this.check7.Size = new System.Drawing.Size(19, 13);
            this.check7.TabIndex = 0;
            this.check7.Text = "✔️";
            this.check7.Visible = false;
            // 
            // check6
            // 
            this.check6.AutoSize = true;
            this.check6.ForeColor = System.Drawing.Color.Lime;
            this.check6.Location = new System.Drawing.Point(17, 184);
            this.check6.Name = "check6";
            this.check6.Size = new System.Drawing.Size(19, 13);
            this.check6.TabIndex = 0;
            this.check6.Text = "✔️";
            this.check6.Visible = false;
            // 
            // check5
            // 
            this.check5.AutoSize = true;
            this.check5.ForeColor = System.Drawing.Color.Lime;
            this.check5.Location = new System.Drawing.Point(17, 156);
            this.check5.Name = "check5";
            this.check5.Size = new System.Drawing.Size(19, 13);
            this.check5.TabIndex = 0;
            this.check5.Text = "✔️";
            this.check5.Visible = false;
            // 
            // check4
            // 
            this.check4.AutoSize = true;
            this.check4.ForeColor = System.Drawing.Color.Lime;
            this.check4.Location = new System.Drawing.Point(17, 128);
            this.check4.Name = "check4";
            this.check4.Size = new System.Drawing.Size(19, 13);
            this.check4.TabIndex = 0;
            this.check4.Text = "✔️";
            this.check4.Visible = false;
            // 
            // check3
            // 
            this.check3.AutoSize = true;
            this.check3.ForeColor = System.Drawing.Color.Lime;
            this.check3.Location = new System.Drawing.Point(17, 100);
            this.check3.Name = "check3";
            this.check3.Size = new System.Drawing.Size(19, 13);
            this.check3.TabIndex = 0;
            this.check3.Text = "✔️";
            this.check3.Visible = false;
            // 
            // check2
            // 
            this.check2.AutoSize = true;
            this.check2.ForeColor = System.Drawing.Color.Lime;
            this.check2.Location = new System.Drawing.Point(17, 72);
            this.check2.Name = "check2";
            this.check2.Size = new System.Drawing.Size(19, 13);
            this.check2.TabIndex = 0;
            this.check2.Text = "✔️";
            this.check2.Visible = false;
            // 
            // check1
            // 
            this.check1.AutoSize = true;
            this.check1.ForeColor = System.Drawing.Color.Lime;
            this.check1.Location = new System.Drawing.Point(17, 46);
            this.check1.Name = "check1";
            this.check1.Size = new System.Drawing.Size(19, 13);
            this.check1.TabIndex = 0;
            this.check1.Text = "✔️";
            this.check1.Visible = false;
            // 
            // spinner
            // 
            this.spinner.Image = ((System.Drawing.Image)(resources.GetObject("spinner.Image")));
            this.spinner.Location = new System.Drawing.Point(11, 36);
            this.spinner.Name = "spinner";
            this.spinner.Size = new System.Drawing.Size(32, 32);
            this.spinner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.spinner.TabIndex = 6;
            this.spinner.TabStop = false;
            // 
            // preInstallationPanel
            // 
            this.preInstallationPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(32)))), ((int)(((byte)(47)))));
            this.preInstallationPanel.Controls.Add(this.label3);
            this.preInstallationPanel.Controls.Add(this.pictureBox3);
            this.preInstallationPanel.Controls.Add(this.label2);
            this.preInstallationPanel.Controls.Add(this.pictureBox2);
            this.preInstallationPanel.Controls.Add(this.btnInstall);
            this.preInstallationPanel.Location = new System.Drawing.Point(10, 12);
            this.preInstallationPanel.Name = "preInstallationPanel";
            this.preInstallationPanel.Size = new System.Drawing.Size(778, 366);
            this.preInstallationPanel.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Raleway", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gainsboro;
            this.label3.Location = new System.Drawing.Point(53, 264);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 11;
            this.label3.Text = "Desarrollado por";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(47, 297);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(128, 39);
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Raleway", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gainsboro;
            this.label2.Location = new System.Drawing.Point(151, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(545, 113);
            this.label2.TabIndex = 9;
            //this.label2.Text = resources.GetString("label2.Text");
            this.label2.Text = label2Text;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(44, 43);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(81, 78);
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // btnInstall
            // 
            this.btnInstall.BackColor = System.Drawing.Color.White;
            this.btnInstall.FlatAppearance.BorderSize = 0;
            this.btnInstall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInstall.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInstall.ForeColor = System.Drawing.Color.Black;
            this.btnInstall.Location = new System.Drawing.Point(426, 283);
            this.btnInstall.Name = "btnInstall";
            this.btnInstall.Size = new System.Drawing.Size(208, 35);
            this.btnInstall.TabIndex = 7;
            this.btnInstall.Text = btnName;
            this.btnInstall.UseVisualStyleBackColor = false;
            this.btnInstall.Click += new System.EventHandler(this.btnInstall_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(42)))), ((int)(((byte)(57)))));
            this.ClientSize = new System.Drawing.Size(800, 390);
            this.Controls.Add(this.preInstallationPanel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button);
            this.Controls.Add(this.txtOutput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Instalador de Ubuntu (" + System.Reflection.Assembly.GetEntryAssembly().GetName().Version + ")";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinner)).EndInit();
            this.preInstallationPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOutput;
        private RoundButton button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblOutput;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label step7;
        private System.Windows.Forms.Label step6;
        private System.Windows.Forms.Label step5;
        private System.Windows.Forms.Label step4;
        private System.Windows.Forms.Label step3;
        private System.Windows.Forms.Label step2;
        private System.Windows.Forms.Label step1;
        
        private System.Windows.Forms.PictureBox spinner;
        private System.Windows.Forms.Label check7;
        private System.Windows.Forms.Label check6;
        private System.Windows.Forms.Label check5;
        private System.Windows.Forms.Label check4;
        private System.Windows.Forms.Label check3;
        private System.Windows.Forms.Label check2;
        private System.Windows.Forms.Label check1;
        private System.Windows.Forms.Panel preInstallationPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private RoundButton btnInstall;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox4;

        public class RoundButton : System.Windows.Forms.Button
        {
            System.Drawing.Drawing2D.GraphicsPath GetRoundPath(System.Drawing.RectangleF Rect, int radius)
            {
                float m = 1.8f;
                float r2 = radius / 2f;
                System.Drawing.Drawing2D.GraphicsPath GraphPath = new System.Drawing.Drawing2D.GraphicsPath();

                GraphPath.AddArc(Rect.X + m, Rect.Y + m, radius, radius, 180, 90);
                GraphPath.AddLine(Rect.X + r2 + m, Rect.Y + m, Rect.Width - r2 - m, Rect.Y + m);
                GraphPath.AddArc(Rect.X + Rect.Width - radius - m, Rect.Y + m, radius, radius, 270, 90);
                GraphPath.AddLine(Rect.Width - m, Rect.Y + r2, Rect.Width - m, Rect.Height - r2 - m);
                GraphPath.AddArc(Rect.X + Rect.Width - radius - m,
                               Rect.Y + Rect.Height - radius - m, radius, radius, 0, 90);
                GraphPath.AddLine(Rect.Width - r2 - m, Rect.Height - m, Rect.X + r2 - m, Rect.Height - m);
                GraphPath.AddArc(Rect.X + m, Rect.Y + Rect.Height - radius - m, radius, radius, 90, 90);
                GraphPath.AddLine(Rect.X + m, Rect.Height - r2 - m, Rect.X + m, Rect.Y + r2 + m);

                GraphPath.CloseFigure();
                return GraphPath;
            }
            protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
            {
                int borderRadius = 35;
                float borderThickness = 1.75f;
                base.OnPaint(e);
                System.Drawing.RectangleF Rect = new System.Drawing.RectangleF(0, 0, this.Width, this.Height);
                System.Drawing.Drawing2D.GraphicsPath GraphPath = GetRoundPath(Rect, borderRadius);

                this.Region = new System.Drawing.Region(GraphPath);
                using (System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Color.White, borderThickness))
                {
                    pen.Alignment = System.Drawing.Drawing2D.PenAlignment.Inset;
                    e.Graphics.DrawPath(pen, GraphPath);
                }
            }
        }
    }
}


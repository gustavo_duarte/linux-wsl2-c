﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Net;
using IWshRuntimeLibrary;
using System.Threading;
using Microsoft.Win32;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Security.Cryptography;



namespace SetupWSL
{
    public partial class Form1 : Form
    {
        public const string UNINSTALL_GUI = "CeibalInstaladorUbuntu";
        public static string applicationName = "InstaladorLinuxWSL2";
        public static string userPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        public static string programFiles = Environment.ExpandEnvironmentVariables("%ProgramW6432%");
        public static string programFilesX86 = Environment.ExpandEnvironmentVariables("%ProgramFiles(x86)%");
        public static string applicationPath = Path.GetFullPath(userPath + "\\" + applicationName);
        public static string installerExePath = applicationPath + "\\Instalador Ubuntu.exe";
        public static string statusFilePath = Path.GetFullPath(applicationPath + "\\status.TMP");
        public static string lockFilePath = Path.GetFullPath(applicationPath + "\\run.lock");
        public static string rebootFilePath = Path.GetTempPath() + "\\reboot.TMP";
        public static string userPathUbuntu = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\.ubuntu";
        public static string logFile = applicationPath + "\\instalador.log";
        public static string assetsURI = "https://apt.ceibal.edu.uy/wsl2-" + System.Reflection.Assembly.GetEntryAssembly().GetName().Version;
        public bool restartNow = false;
        private States states;
        private bool forceUninstall = false;
        private static Process psId = null;
        private static bool reintent = false;
        private static bool disconnected = false;
        private static bool showDisconnected = false;
        //private static string md5Ubuntu = "e13b99fd6ac5d9985a570ec77c812ec2";



        public Form1 (bool resume = false, bool uninstall = false)
        {
            // Oculta la barra superior.
            //this.ControlBox = false;

            //if (System.IO.File.Exists(lockFilePath))
            //{
            //    Environment.Exit(0);
            //    return;
            //}
            //else
            //{
            //    Directory.CreateDirectory(applicationPath);
            //    System.IO.File.WriteAllText(lockFilePath, "");
            //}
            
            Log("Inicio");
            restartNow = false;
            this.states = new States(this);
            string btnName = "INSTALAR";
            string label2Text = "El proceso de instalación puede llevar hasta 30 minutos y ocupará 8GB de tu disco. Antes de continuar, asegúrate de que tu equipo esté conecatdo a la corriente y de que cuentas con conexión a Internet estable. Presiona en 'Instalar' para continuar o 'Cancelar' para salir de la instalación.";

            
            if (resume)
            {
                Log("Resume");
                btnName = "CONTINUAR";
            }
            else if (this.states.GetStatus() == (int)States.Name.Complete || uninstall)
            {
                this.forceUninstall = true;
                btnName = "DESINSTALAR";
                label2Text = "Tu equipo ya tiene instalado el virtualizador de Ubuntu. " +
                    "Si deseas desinstalarlo, el proceso puede demorar unos minutos. Luego de realizada la desinstalación, es necesario que reinicies el equipo.";
            }
            else
            {
                btnName = "INSTALAR";
            }
            InitializeComponent(btnName, label2Text);
            CheckConnectionTask();
        }

   
        public void Log(string line)
        {
            line = DateTime.Now.ToString("O") + " " + line;
            if (!Directory.Exists(applicationPath))
            {
                Directory.CreateDirectory(applicationPath);
            }

            if (!Directory.Exists(userPathUbuntu))
            {
                Directory.CreateDirectory(userPathUbuntu);
            }

            if (!System.IO.File.Exists(logFile))
            {
                using (StreamWriter sw = System.IO.File.CreateText(logFile))
                {
                    sw.WriteLine(line);
                    return;
                }
            }

            using (StreamWriter sw = System.IO.File.AppendText(logFile))
            {
                sw.WriteLine(line);
                return;
            }
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                //HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://apt.ceibal.edu.uy");
                //request.Timeout = 10000;
                //request.ReadWriteTimeout = 1000;
                //request.GetResponse();
                using (var client = new WebClient())
                using (var stream = client.OpenRead("https://apt.ceibal.edu.uy"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool CheckForInternetConnection(int timeoutMs = 10000)
        {
            try
            {
                var url = "https://apt.ceibal.edu.uy";

                var request = (HttpWebRequest)WebRequest.Create(url);
                request.KeepAlive = false;
                request.Timeout = timeoutMs;
                using (var response = (HttpWebResponse)request.GetResponse())
                    return true;
            }
            catch
            {
                return false;
            }
        }

        public void CheckConnectionTask()
        {
            Task.Run(() => {
                while (true)
                    {
                        if (!CheckForInternetConnection(5000))
                        {
                            Log("Desconexion detectada en: " + states.GetStatus());
                            PrintOutput("Verifique la connexión a internet", true);
                            showDisconnected = true;
                            reintent = false;
                            if (states.GetStatus() != (int)States.Name.SoundConfigured && 
                                states.GetStatus() != (int)States.Name.Ceibalizacion &&
                                states.GetStatus() != (int)States.Name.UbuntuDesktopInstalled) 
                            {
                                Form1.disconnected = true;
                                if (this.states.wc != null)
                                {
                                    this.states.wc.CancelAsync();
                                }
                                if (psId != null)
                                {
                                    psId.Kill();
                                    Log("Proceso stopeado: " + states.GetStatus());
                                }

                            }
                        }
                        else
                        {
                            Form1.disconnected = false;
                            if (showDisconnected)
                            { 
                                Log("Conexion establecida en:" + states.GetStatus());
                                reintent = true;
                                showDisconnected = false;
                                PrintOutput("Conexion establecida");
                                if (states.GetStatus() != (int)States.Name.SoundConfigured && 
                                    states.GetStatus() != (int)States.Name.Ceibalizacion &&
                                    states.GetStatus() != (int)States.Name.UbuntuDesktopInstalled)
                                {
                                    Invoke((MethodInvoker)delegate
                                    {
                                        button.Text = "REINTENAR";
                                    });
                                }
                            }
                        }
                        Thread.Sleep(10000);
                    }
                }
            );
           
        }

        private void StartInstallation() 
        {
            Form1.disconnected = false;
            states.init();
            states.next();            
        }

        private void StartDesinstallation()
        {
            Invoke((MethodInvoker)delegate
            {
                button.Hide();
            });

            DialogResult dialogResult = MessageBox.Show("Confirma que desea desinstalar ?", "", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                // Continuo
            }
            else if (dialogResult == DialogResult.No)
            {
                //states.RunPsCmd("Remove-Item -Path " + lockFilePath + " -Force -ErrorAction SilentlyContinue");
                Environment.Exit(0);
            }

            states.AdvanceStep(7);
            PrintOutput("Comienza la desinstalación");

            PrintOutput("Elminando archivos del instalador");
            
            states.RunPsCmd("Remove-Item -Path " + Path.GetTempPath() + "\\CanonicalGroupLimited.Ubuntu20.04onWindows.appx" + " -Force -ErrorAction SilentlyContinue");
            states.RunPsCmd("Remove-Item -Path " + Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Desktop\\Ubuntu.lnk" + " -Force -Recurse -ErrorAction SilentlyContinue");

            PrintOutput("Desinstalando Appx de Ubuntu");
            states.RunPsCmd("Get-AppxPackage *ubuntu* | Remove-AppxPackage -AllUsers");

            PrintOutput("Desinstalando Xserver");
            states.RunPsCmd("choco uninstall vcxsrv -y");

            PrintOutput("Deteniendo servicio de sonido");
            try
            {
                states.RunPsCmd("Stop-Service -Name pulseaudio");
            }
            catch (Exception o)
            {
                PrintOutput(o.Message);
            }

            PrintOutput("Eliminando servicio de sonido");

            try
            {
                states.RunPsCmd("C:\\pulse\\nssm.exe remove pulseaudio confirm");
                //States.RunPsCmd("Remove-Service -Name pulseaudio");
            
                states.RunPsCmd("Remove-Item -Path 'c:\\pulse' -Force -Recurse -ErrorAction SilentlyContinue");
                PrintOutput("Removiendo servicio de monitoreo");
                states.RunPsCmd("Unregister-ScheduledTask -TaskName WslMonitor -Confirm:$false");

                PrintOutput("Removiendo llaves de registro");
                unregisterUninstaller();
            }
            catch (Exception o)
            {
                PrintOutput(o.Message);
            }
            states.EndStep(7);
            
            PrintOutput("Finalizó de desinstalación, debe reiniciar el sistema");
            states.RunPsCmd("Remove-Item -Path " + applicationPath + " -Force -Recurse -ErrorAction SilentlyContinue");
            Invoke((MethodInvoker)delegate
            {
                restartNow = true;
                button.Show();
                button.Text = "Reiniciar ahora";
            });

        } 

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (reintent == true) 
            {
                reintent = false;
                Task.Run(() =>
                {
                    
                    Invoke((MethodInvoker)delegate
                    {
                        button.Text = "CANCELAR";
                    });
                    StartInstallation();
                });
                return;
            }
            if (this.restartNow)
            {
                Process.Start("shutdown.exe", "-r -t 3");
            }
            //states.RunPsCmd("Remove-Item -Path " + lockFilePath + " -Force -ErrorAction SilentlyContinue");
            Environment.Exit(0);
        }

        private void btnInstall_Click(object sender, EventArgs e)
        {
            preInstallationPanel.Visible = false;
            Task.Run (() => {
                try
                {
                    if ((this.states.GetStatus() == (int)States.Name.Complete) || this.forceUninstall)
                    {
                        StartDesinstallation();
                    }
                    else
                    {
                        StartInstallation();
                    }
                    
                }
                catch (Exception o)
                {
                    PrintOutput(o.Message, true);
                }
            });
        }

        private void PrintOutput(string msg, bool error = false)
        {
            lblOutput.Invoke((MethodInvoker)delegate {
                lblOutput.Text = msg;
                if (error) lblOutput.ForeColor = Color.FromName("Red");
                else lblOutput.ForeColor = Color.FromName("White");
            });
            Thread.Sleep(2000);
        }

        private static bool registerUninstaller()
        {
           
            using (RegistryKey parent = Registry.LocalMachine.OpenSubKey(
                @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", true))
            {
                if (parent == null) return false;
                try
                {
                    // use custom gui string
                    RegistryKey key = null;
                    key = parent.OpenSubKey(UNINSTALL_GUI, true) ?? parent.CreateSubKey(UNINSTALL_GUI);
                    if (key == null) return false;
                    key.SetValue("DisplayName", "Linux Ubuntu Ceibal");
                    key.SetValue("ApplicationVersion", "1.0");
                    key.SetValue("Publisher", "Ceibal");
                    key.SetValue("DisplayIcon", $"{userPathUbuntu}\\ubuntu.ico");
                    key.SetValue("DisplayVersion", "1.0");
                    key.SetValue("URLInfoAbout", "https://apt.ceibal.edu.uy/wsl2-");
                    key.SetValue("Contact", "https://apt.ceibal.edu.uy/wsl2-");
                    key.SetValue("InstallDate", DateTime.Now.ToString("yyyyMMdd"));
                    key.SetValue("UninstallString", $@"{installerExePath} -uninstall");

                    var currentPath = System.Reflection.Assembly.GetEntryAssembly().Location;
                    if (System.IO.File.Exists(installerExePath)) {
                        System.IO.File.Delete(installerExePath);
                    }
                    System.IO.File.Copy(currentPath, installerExePath);
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        private static bool unregisterUninstaller()
        {
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(
               @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", true))
            {
                try
                {
                    if (key != null)
                    {
                        key.DeleteSubKey(UNINSTALL_GUI);
                        return true;
                    }
                    else return false;
                }
                catch
                {
                    return false;
                }
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        class States
        {
            public enum Name
            {
                Init = 0,
                RegisterUninstaller,
                Wsl2Enabled,
                VirtualPlatformEnabled,
                KernelUpdated,
                wsl2Version,
                ChcolateyInstalled,
                XsrvInstalled,
                UbuntuInstalled,
                UbuntuUpdated,
                UbuntuConfigured,
                SoundConfigured,
                UbuntuDesktopInstalled,
                Ceibalizacion,
                Complete
            }
            private readonly Dictionary<States.Name, Func<bool>> statesTask;
            private static Form1 form1;
            private string DistroFileName = Path.GetFullPath(Path.GetTempPath() + "\\CanonicalGroupLimited.Ubuntu20.04onWindows.appx");
            public WebClient wc;

            public States(Form1 f)
            {
                States.form1 = f;
                
                this.statesTask = new Dictionary<Name, Func<bool>>
                {
                    [States.Name.Init] = enableWsl2,
                    [States.Name.Wsl2Enabled] = enableVm,
                    [States.Name.VirtualPlatformEnabled] = updateKernel,
                    [States.Name.KernelUpdated] = setWslVersion2,
                    [States.Name.wsl2Version] = installChocolatey,
                    [States.Name.ChcolateyInstalled] = installXserver,
                    [States.Name.XsrvInstalled] = installUbuntu,
                    [States.Name.UbuntuInstalled] = updateUbuntu,
                    [States.Name.UbuntuUpdated] = configureUbuntu,
                    [States.Name.UbuntuConfigured] = configureSoundServer,
                    [States.Name.SoundConfigured] = installDesktop,
                    [States.Name.UbuntuDesktopInstalled] = ceibalization,
                    [States.Name.RegisterUninstaller] = registerUninstaller,
                    [States.Name.Ceibalizacion] = registerAndClean,
                    [States.Name.Complete] = complete
                };
            }

            public void next()
            {
                var currentState = GetStatus();
                States.form1.Log("Status inicial: " + currentState);
                changeStep((Name)currentState);
                
                // Apago la vm, para eliminar procesos zombies. 
                if (currentState > (int)States.Name.XsrvInstalled)
                {
                    RunWslCmd("--shutdown");
                }
                
                while ((Name)currentState <= Name.Complete && Form1.disconnected == false && statesTask[(Name)currentState]())
                {
                    if (currentState < (int)States.Name.Complete && Form1.disconnected == false)
                    {
                        currentState = currentState + 1;
                        changeStep((Name)currentState);
                        SaveStatus((Name)currentState);
                        States.form1.Log("Nuevo Status: " + currentState);
                    }
                }
                States.form1.Log("salgo de NEXT");
            }

            private void changeStep(Name currentState)
            {
                if (currentState < Name.ChcolateyInstalled)
                {
                    AdvanceStep(1);
                }
                else if (currentState < Name.UbuntuInstalled)
                {
                    AdvanceStep(2);
                }
                else if (currentState < Name.SoundConfigured)
                {
                    AdvanceStep(3);
                }
                else if (currentState < Name.UbuntuDesktopInstalled)
                {
                    AdvanceStep(4);
                }
                else if (currentState < Name.Ceibalizacion)
                {
                    AdvanceStep(5);
                }
                else if (currentState == Name.Complete)
                {
                    EndStep(6);
                }
            }

            public string RunPsCmd(string cmd)
            {
				return RunCmd("powershell.exe", $"-NoProfile -ExecutionPolicy ByPass -Command \"{cmd}\"");
            }
			
            private string RunWslCmd(string cmd)
            {
				var res = RunCmd("C:\\Windows\\Sysnative\\wsl.exe", cmd);
                string strRegex = @"El subsistema de Windows para Linux no tiene distribuciones instaladas";
                Regex re = new Regex(strRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                var match = re.Match(res);
                if (match.Success)
                {
                    throw new Exception(res);
                }
                else 
                {
                    return res;
                }
            }

            private string RunCmd(string execPath, string cmd, bool IgnoreError=false)
            {

                var processStartInfo = new ProcessStartInfo
                {
                    FileName = execPath,
                    Arguments = cmd,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                };
                psId = Process.Start(processStartInfo);
                var output = psId.StandardOutput.ReadToEnd().Replace("\x00", "");
                var error = psId.StandardError.ReadToEnd().Replace("\x00", "").Replace("\n", "#"); 
                psId.WaitForExit();
                psId = null;
                States.form1.Log("");
                States.form1.Log("");
                States.form1.Log("--------------------------------------------------------------------------");
                States.form1.Log("CMD: " + cmd);
                States.form1.Log("OUTPUT: " + output);
                States.form1.Log("ERROR: " + error);
                States.form1.Log("--------------------------------------------------------------------------");
                States.form1.Log("");
                States.form1.Log("");
                if (error.Length > 0 && !IgnoreError)
                {
                    throw new Exception(error);
                }
                else 
                {
                    return output;
                }
                
            }

            public void AdvanceStep(int step)
            {
                Label next = (Label)States.form1.Controls.Find($"step{step}", true).First();
                States.form1.Invoke((MethodInvoker)delegate
                {
                    next.ForeColor = Color.FromName("White");
                    States.form1.spinner.Top = next.Top - 10;
                    if (step > 1)
                    {
                        for (var i = 1; i < step; i++)
                        {
                            Label prev = (Label)States.form1.Controls.Find($"step{step - i}", true).First();
                            Label prevCheck = (Label)States.form1.Controls.Find($"check{step - i}", true).First();
                            prev.ForeColor = Color.FromName("Gray");
                            prevCheck.Visible = true;
                        }
                    }
                });
            }

            public void EndStep(int step)
            {
                States.form1.Invoke((MethodInvoker)delegate
                {
                    States.form1.spinner.Hide();
                    if (step > 1)
                    {
                        for (var i = 0; i < step; i++)
                        {
                            Label prev = (Label)States.form1.Controls.Find($"step{step - i}", true).First();
                            Label prevCheck = (Label)States.form1.Controls.Find($"check{step - i}", true).First();
                            prev.ForeColor = Color.FromName("Gray");
                            prevCheck.Visible = true;
                        }
                    }
                });
            }

            private void checkHDSpace()
            {
                States.form1.PrintOutput("Verificando espacio libre...");
                DriveInfo[] allDrives = DriveInfo.GetDrives();
                foreach (DriveInfo d in allDrives)
                {
                    if (d.IsReady == true)
                    {
                        if (d.Name == "C:\\")
                        {
                            if (d.AvailableFreeSpace < (8L * 1024L * 1024L * 1024L))
                            {
                                throw new Exception("El espacio libre del disco es " + d.AvailableFreeSpace / (1024 * 1024 * 1024) + "G bytes, se requiere mas de 8G bytes.");
                            }
                        }
                    }
                }
            }

            private string parseResponse(string field, string response) {
                string strRegex = @"(\b" + field + @"\b)\s*(:)(\s*)(\w*)";
                Regex re = new Regex(strRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                var match = re.Match(response);
                if (match.Success) 
                {
                    Group g = match.Groups[4];
                    CaptureCollection c = g.Captures;
                    return c[0].Value;
                }
                return "";
            }

            private void checkWindowsVersion()
            {
                var res = RunPsCmd("(Get-ItemProperty 'HKLM:\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion')");
                if (Int32.Parse(parseResponse("CurrentBuild", res)) < 19043)
                {
                    throw new Exception("Se requiere Windows 10, build mayor a  19043, verifique si tiene actualizaciones pendientes");
                }
            }

            private string getSerialNumber()
            {
                string strRegex = @"96\w*";
                Regex re = new Regex(strRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                var sn = RunPsCmd("Get-WmiObject win32_bios | select Serialnumber");
                var match = re.Match(sn);
                return match.Value;        
            }

            private void checkRequirements()
            {
                checkHDSpace();
                checkWindowsVersion();
                setTime();
            }

            private void setTime() 
            {
                States.form1.PrintOutput("Configurando zona horaria");
                RunPsCmd("Set-TimeZone -Id 'Montevideo Standard Time'");
                States.form1.PrintOutput("Sincronizando el reloj");
                try
                {
                    RunPsCmd("net stop w32time");
                }
                catch (Exception e)
                { 
                    // el servicio no estaba corriendo, no hacemos nada.
                }
                RunPsCmd("w32tm /unregister");
                RunPsCmd("w32tm /register");
                RunPsCmd("net start w32time");
                RunPsCmd("w32tm /resync /force");
            }
            public int GetStatus()
            {
                if (System.IO.File.Exists(statusFilePath) == false)
                {
                    Directory.CreateDirectory(applicationPath);
                    SaveStatus(States.Name.Init);
                }
                int status = int.Parse(System.IO.File.ReadLines(statusFilePath).First());
                return status;
            }

            private void SaveStatus(States.Name status)
            {
                System.IO.File.WriteAllText(statusFilePath, ((int)status).ToString());
            }

            //////////////////////////////////////////////////////////////
            //
            //  STATES TASKS
            //
            //////////////////////////////////////////////////////////////
            public bool init()
            {
                this.checkRequirements();
                return true;
            }

            // State 1
            private bool enableWsl2()
            {
                States.form1.PrintOutput("Habilitando WSL2");
                var res = RunPsCmd("Enable-WindowsOptionalFeature -Online -NoRestart -FeatureName Microsoft-Windows-Subsystem-Linux");
                if (Boolean.Parse(parseResponse("RestartNeeded", res)))
                {

                    States.form1.PrintOutput("!!! Es neceario reiniciar el PC para continuar la instalacion !!!!");
                    RegistryKey rk = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce", true);
                    rk.SetValue("!UbuntuInstaller", Application.ExecutablePath + " -resume");
                    States.form1.Invoke((MethodInvoker)delegate
                    {
                        States.form1.restartNow = true;
                        States.form1.button.Text = "Reiniciar ahora";
                    });
                    return false;
                }
                else
                {
                    States.form1.PrintOutput("WSL2 habilitado");
                    return true;
                }
            }

            // State 2
            private bool enableVm()
            {
                States.form1.PrintOutput("Habilitando plataforma de máquina virtual");
                var res = RunPsCmd("Enable-WindowsOptionalFeature -Online -NoRestart -FeatureName VirtualMachinePlatform");
                if (Boolean.Parse(parseResponse("RestartNeeded", res)))
                {
                    States.form1.PrintOutput("¡Es neceario reiniciar el PC para continuar la instalación!");
                    RegistryKey rk = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce", true);
                    rk.SetValue("!UbuntuInstaller2", Application.ExecutablePath + " -resume");
                    States.form1.Invoke((MethodInvoker)delegate
                    {
                        States.form1.restartNow = true;
                        States.form1.button.Text = "Reiniciar ahora";
                    });
                    return false;
                }
                else
                {
                    States.form1.PrintOutput("Platforma de máquina virtual habilitada");
                    return true;
                }
            }

            // State 3
            private bool updateKernel()
            {
                var resultError = false;
                States.form1.PrintOutput("Verificando la actualización del kernel WSL2");
                var res = RunPsCmd("Get-ChildItem -Path 'HKLM:\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall' | ForEach-Object { Get-ItemProperty $_.PSPath } | Select-Object DisplayName");
                
                string strRegex = @"Linux Update";
                Regex re = new Regex(strRegex, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                var match = re.Match(res);
                if (match.Success) 
                {
                    States.form1.PrintOutput("Ya instalado");
                    return true;
                }
                
                States.form1.PrintOutput("Descargando actualización del kernel WSL2");
                States.form1.Invoke((MethodInvoker)delegate
                {
                    States.form1.progressBar.Visible = true;
                    States.form1.progressBar.Value = 0;
                });

                using (wc = new WebClient())
                {
                    wc.DownloadProgressChanged += (o, e) => States.form1.progressBar.Invoke((MethodInvoker)delegate
                    { 
                        if (disconnected)
                        {
                            wc.CancelAsync();
                        }
                        States.form1.progressBar.Value = e.ProgressPercentage;
                    });
                    wc.DownloadFileCompleted += (o, e) =>
                    {
                        if (e.Cancelled)
                        {
                            //cleanup delete partial file
                            wc.Dispose();
                            States.form1.PrintOutput("Descarga cancelada", true);
                            resultError = true;
                        }
                        else if (e.Error != null)
                        {
                            States.form1.PrintOutput(e.Error.Message, true);
                            resultError = true;
                        }
                        lock (e.UserState) Monitor.Pulse(e.UserState);
                        States.form1.progressBar.Invoke((MethodInvoker)delegate
                        {
                            States.form1.progressBar.Visible = false;
                        });
                        
                    };
                    var syncObject = new Object();
                    lock (syncObject)
                    {
                        wc.DownloadFileAsync(new Uri("https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi"),
                            Application.StartupPath + "\\wsl_update_x64.msi", syncObject);
                        Monitor.Wait(syncObject);
                    }
                }
                States.form1.Invoke((MethodInvoker)delegate
                {
                    States.form1.progressBar.Visible = false;
                });
                if (resultError)
                {
                    throw new Exception("Error en la descarga");
                }
                States.form1.PrintOutput("Instalando actualización del kernel WSL2");
                RunPsCmd($"msiexec /i {Application.StartupPath + "\\wsl_update_x64.msi"} /qn");
                Thread.Sleep(30000);
                States.form1.PrintOutput("Fin de actualización del kernel WSL2");
                return true;
                
            }

            // State 4
            private bool setWslVersion2()
            {
                States.form1.PrintOutput("Configurando versión 2 de WSL2 por defecto");
                RunWslCmd("--set-default-version 2");
                return true;
            }

            // State 5
            private bool installChocolatey()
            {
                States.form1.PrintOutput("Verificando la instalación de Chocolatey");
                
                RunPsCmd("Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))");
                return true;
            }

            // State 6
            private bool installXserver()
            {
                States.form1.PrintOutput("Verificando la instalación de Xserver");
                RunPsCmd("Remove-Item -Path 'C:\\ProgramData\\chocolatey\\lib' -Force -Recurse -ErrorAction SilentlyContinue");
                RunPsCmd(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\chocolatey\\bin\\choco.exe install vcxsrv -y --no-progress");
                System.IO.File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory) + "\\XLaunch.lnk");
                return true;
            }

            // State 7
            private void downloadUbuntu()
            {
                var resultError = false;
                States.form1.PrintOutput($"Inicio descarga de la distro Ubuntu 20.04");
                var md5sumGuardado = (new WebClient()).DownloadString("https://apt.ceibal.edu.uy/wsl2/wslubuntu2004.md5sum").Replace("\n", "");
           
                States.form1.Invoke((MethodInvoker)delegate
                {
                    States.form1.progressBar.Visible = true;
                    States.form1.progressBar.Value = 0;
                });
                using (wc = new WebClient())
                {
                    wc.DownloadProgressChanged += (o, e) => States.form1.progressBar.Invoke((MethodInvoker)delegate
                    {
                        States.form1.progressBar.Value = e.ProgressPercentage;
                    });
                    wc.DownloadFileCompleted += (o, e) =>
                    {
                        if (e.Cancelled)
                        {
                            //cleanup delete partial file
                            wc.Dispose();
                            States.form1.PrintOutput("Descarga cancelada", true);
                            resultError = true;
                        } 
                        else if (e.Error != null)
                        {
                            States.form1.PrintOutput(e.Error.Message, true);
                            resultError = true;
                        }
                        
                        lock (e.UserState) Monitor.Pulse(e.UserState);
                        
                        States.form1.progressBar.Invoke((MethodInvoker)delegate
                        {
                            States.form1.progressBar.Visible = false;
                        });
                       

                    };
                    var syncObject = new Object();
                    lock (syncObject)
                    {
                        wc.DownloadFileAsync(new Uri("https://apt.ceibal.edu.uy/wsl2/wslubuntu2004"), DistroFileName, syncObject);
                        //wc.DownloadFileAsync(new Uri("https://aka.ms/wslubuntu2004"), DistroFileName, syncObject);
                        Monitor.Wait(syncObject);
                    }
                }
                if (resultError) {
                    throw new Exception("Error en la descarga");
                }
                States.form1.PrintOutput("Calculando MD5.");
                using (var md5Instance = MD5.Create())
                {
                    using (var stream = System.IO.File.OpenRead(DistroFileName))
                    {
                        var hashResult = md5Instance.ComputeHash(stream);
                        var md5sumDescarga = BitConverter.ToString(hashResult).Replace("-", "").ToLowerInvariant();
                        if (md5sumDescarga != md5sumGuardado)
                        {
                            throw new Exception("MD5 no coincide.");
                        }
                    }
                }
                return;
            }

            // State 8
            private bool installUbuntu()
            {
                States.form1.PrintOutput("Verificando la instalación de Ubuntu 20.04");
                try
                {
                    var res = RunPsCmd("Get-AppxPackage -AllUsers -Name *ubuntu20.04*");
                    if (parseResponse("Status", res) == "Ok")
                    {
                        States.form1.PrintOutput("Ubuntu 20.04 ya instalado");
                        return true;
                    }
                    downloadUbuntu();
                }
                catch (Exception e)
                {
                    States.form1.PrintOutput(e.Message, true);
                    return false;
                }

               

                States.form1.PrintOutput("Agreagando Appx de Ubuntu 20.04");
                try
                {
                    RunPsCmd($"Add-AppxPackage -Path \"" + DistroFileName + "\"");
                }
                catch (Exception e)
                {
                    States.form1.PrintOutput(e.Message);
                    Thread.Sleep(3000);
                    States.form1.PrintOutput("Intentando resolver el error");
                    if (e.Message.Contains("HRESULT"))
                    {
                        RunPsCmd("dism /online /cleanup-image /restorehealth");
                        RunPsCmd("sfc /scannow");
                        RunPsCmd($"Add-AppxPackage -Path \"" + DistroFileName + "\"");
                    }
                    else
                    {
                        throw new Exception(e.Message);
                    }
                }

                RunPsCmd("ubuntu.exe install --root");
                var result = RunWslCmd($"-e uname");
                if (result.ToString().Contains("Linux"))
                {
                    States.form1.PrintOutput("Finalizó la instalación de Ubuntu 20.04");
                    return true;
                }
                else
                {
                    throw new Exception("Falló la isntalación de la disribución de Ubuntu");
                }
            }

            // State 9
            private bool updateUbuntu()
            {
                States.form1.PrintOutput("Escribiendo el Serial Number");
                RunWslCmd($"sh -c \"echo {getSerialNumber()} > /etc/ceibal_sn\"");

                using (var wc = new WebClient())
                {
                    wc.DownloadFile(new Uri(assetsURI + "/linux_cmds.sh"), $"{userPathUbuntu}\\linux_cmds.sh");
                }
                States.form1.PrintOutput("Actualizando paquetes de Ubuntu");
                RunWslCmd($"sh -c \"/mnt/c/users/{Environment.UserName}/.ubuntu/linux_cmds.sh update_pkgs\"");
                return true;
            }

            // State 10
            private bool configureUbuntu()
            {
                using (var wc = new WebClient())
                {
                    wc.DownloadFile(new Uri(assetsURI + "/linux_cmds.sh"), $"{userPathUbuntu}\\linux_cmds.sh");
                }

                States.form1.PrintOutput("Instalando Utils");
                var utils = RunWslCmd($"sh -c \"/mnt/c/users/{Environment.UserName}/.ubuntu/linux_cmds.sh install_utils\"");
                if (utils.ToString().Contains("ERROR"))
                {
                    States.form1.PrintOutput(utils[0].ToString());
                    return false;
                }

                States.form1.PrintOutput("Descargando paquetes de sonido");
                RunPsCmd("Remove-Item -Path " + Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\pulse* -Force -Recurse -ErrorAction SilentlyContinue");
                RunPsCmd("Remove-Item -Path " + Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\nssm* -Force -Recurse -ErrorAction SilentlyContinue");
                RunPsCmd("Remove-Item -Path 'c:\\pulse' -Force -Recurse -ErrorAction SilentlyContinue");

                var sound = RunWslCmd($"sh -c \"/mnt/c/users/{Environment.UserName}/.ubuntu/linux_cmds.sh sound\"");
                if (sound.ToString().Contains("ERROR"))
                {
                    States.form1.PrintOutput(sound[0].ToString());
                    return false;
                }

                States.form1.PrintOutput("Instalando Genie Systemd");
                var genie = RunWslCmd($"sh -c \"/mnt/c/users/{Environment.UserName}/.ubuntu/linux_cmds.sh install_genie\"");
                if (genie.ToString().Contains("ERROR"))
                {
                    States.form1.PrintOutput(genie[0].ToString());
                    return false;
                }
                return true;
            }

            // State 11
            private bool configureSoundServer()
            {
                States.form1.PrintOutput("Configurando server de Sonido");
                RunPsCmd($"c:\\pulse\\nssm.exe install pulseaudio c:\\pulse\\pulseaudio.exe");
                RunPsCmd($"c:\\pulse\\nssm.exe set pulseaudio application c:\\pulse\\pulseaudio.exe");
                RunPsCmd($"c:\\pulse\\nssm.exe set pulseaudio appdirectory c:\\pulse");
                RunPsCmd($"c:\\pulse\\nssm.exe set pulseaudio appparameters --file c:\\pulse\\config.pa");
                RunPsCmd($"c:\\pulse\\nssm.exe set pulseaudio displayname pulseaudio");
                RunPsCmd($"net start pulseaudio");
                return true;
            }

            // State 12
            private bool installDesktop()
            {
                using (var wc = new WebClient())
                {
                    wc.DownloadFile(new Uri(Form1.assetsURI + "/linux_cmds.sh"), $"{userPathUbuntu}\\linux_cmds.sh");
                }
                States.form1.PrintOutput("Instalando Ubuntu Desktop");
                var mate = RunWslCmd($"sh -c \"/mnt/c/users/{Environment.UserName}/.ubuntu/linux_cmds.sh install_gui\"");
                if (mate.ToString().Contains("ERROR"))
                {
                    States.form1.PrintOutput(mate[0].ToString());
                    return false;
                }
                return true;
            } 
            
            // State 13
            private bool ceibalization()
            {
                using (var wc = new WebClient()) {
                    wc.DownloadFile(new Uri(Form1.assetsURI + "/linux_cmds.sh"), $"{userPathUbuntu}\\linux_cmds.sh");
                    wc.DownloadFile(new Uri(Form1.assetsURI + "/01_reload_vcxsrv.ps1"), $"{userPathUbuntu}\\01_reload_vcxsrv.ps1");
                    wc.DownloadFile(new Uri(Form1.assetsURI + "/02_start_desktop.sh"), $"{userPathUbuntu}\\02_start_desktop.sh");
                    wc.DownloadFile(new Uri(Form1.assetsURI + "/03_start_desktop.vbs"), $"{userPathUbuntu}\\03_start_desktop.vbs");
                    wc.DownloadFile(new Uri(Form1.assetsURI + "/ubuntu.ico"), $"{userPathUbuntu}\\ubuntu.ico");
                }

                States.form1.PrintOutput("Creando usuario Ceibal");
                var ceibal = RunWslCmd($"sh -c \"/mnt/c/users/{Environment.UserName}/.ubuntu/linux_cmds.sh user_ceibal\"");
                if (ceibal.ToString().Contains("ERROR"))
                {
                    States.form1.PrintOutput(ceibal[0].ToString());
                    return false;
                }

                States.form1.PrintOutput("Deshabilitando servicios de Linux");
                var services = RunWslCmd($"sh -c \"/mnt/c/users/{Environment.UserName}/.ubuntu/linux_cmds.sh services\"");
                if (services.ToString().Contains("ERROR"))
                {
                    States.form1.PrintOutput(services[0].ToString());
                    return false;
                }

                States.form1.PrintOutput("Configurando ceibal como usuario por defecto...");
                RunPsCmd("ubuntu.exe config --default-user ceibal");

                States.form1.PrintOutput("Creando acceso directo");
                string shortcutLocation = $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}\\Desktop\\Ubuntu.lnk";
                string programLocation = Path.GetFullPath(userPathUbuntu + "\\03_start_desktop.vbs");
                string iconLocation = Path.GetFullPath(userPathUbuntu + "\\ubuntu.ico");

                WshShell shell = new WshShell();
                IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutLocation);
                shortcut.TargetPath = programLocation;
                shortcut.IconLocation = iconLocation;
                shortcut.Save();
                
                States.form1.PrintOutput("Agregando reglas en Firewall");
                RunPsCmd($"New-NetFirewallRule -DisplayName \"Xserver1\" -Direction Inbound -Program \'{programFiles}\\VcXsrv\\vcxsrv.exe\' -Action Allow");
                RunPsCmd($"New-NetFirewallRule -DisplayName \"Xserverx86\" -Direction Inbound -Program \'{programFilesX86}\\VcXsrv\\vcxsrv.exe\' -Action Allow");
                RunPsCmd($"new-netfirewallrule -displayname \"pulseaudio\" -direction inbound -program \"c:\\pulse\\pulseaudio.exe\" -profile any -action allow");
                return true;
            }

            // State 14
            private bool registerAndClean()
            {
                States.form1.PrintOutput("Registrando servicio de monitoreo");
               
                RunPsCmd($"$task=Register-ScheduledTask -Action (New-ScheduledTaskAction -Execute \'{installerExePath}\' -Argument '-monitor') -Trigger (New-ScheduledTaskTrigger -Daily -At 12am) -TaskName 'WslMonitor' -RunLevel Highest; $task.Triggers.Repetition.Duration = 'P365D'; $task.Triggers.Repetition.Interval = 'PT1M'; $task | Set-ScheduledTask");

                System.IO.File.Delete(Application.StartupPath + "\\wsl_update_x64.msi");
                RunWslCmd("--shutdown");
                States.form1.Invoke((MethodInvoker)delegate {
                    States.form1.spinner.Visible = false;
                    States.form1.step5.ForeColor = Color.FromName("Gray");
                    States.form1.check5.Visible = true;
                    //States.form1.button.Hide();
                    States.form1.button.Text = "CERRAR";

                });
                return true;
            }
            
            // State 15
            private bool complete()
            {
                RunPsCmd("Remove-Item -Path " + Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\pulse* -Force -Recurse -ErrorAction SilentlyContinue");
                RunPsCmd("Remove-Item -Path " + Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads\\nssm* -Force -Recurse -ErrorAction SilentlyContinue");
                //RunPsCmd("Remove-Item -Path " + lockFilePath + " -Force -ErrorAction SilentlyContinue");
                States.form1.PrintOutput("Fin de la instalación.");
                return true;
            }
        }
    }
}

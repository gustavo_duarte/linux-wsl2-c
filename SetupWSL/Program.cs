﻿using Microsoft.Win32;
using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;


namespace SetupWSL
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        // if you want to allow only one instance otherwise remove the next line
        static Mutex mutex = new Mutex(false, "CEIBAL-UBUNTU-WSL-INSTALADOR");
        [STAThread]
        static void Main(string[] args)
        {
            // if you want to allow only one instance otherwise remove the next 4 lines
            if (!mutex.WaitOne(TimeSpan.FromSeconds(2), false))
            {
                return; // singleton application already started
            }
            try
            {
                // Apply config           
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                if (args.Length > 0 && args[0] is "-resume")
                {
                    Application.Run(new Form1(true));
                }
                else if (args.Length > 0 && args[0] is "-uninstall")
                {
                    Application.Run(new Form1(false, true));
                }
                else if (args.Length > 0 && args[0] is "-monitor")
                {
                    Monitor();
                }
                else
                {
                    Application.Run(new Form1());
                }
            }
            finally 
            {
                // if you want to allow only one instance otherwise remove the next line
                mutex.ReleaseMutex();
            }
            

        }

        static string GetProcessOutput(string cmd)
        {
            var processStartInfo = new ProcessStartInfo
            {
                FileName = "C:\\Windows\\Sysnative\\wsl.exe",
                Arguments = cmd,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };
            var process = Process.Start(processStartInfo);
            var output = process.StandardOutput.ReadToEnd().Replace("\x00", ""); ;
            var error = process.StandardError.ReadToEnd();
            process.WaitForExit();
 
            if (error.Length > 0) return error;
            else return output;
        }
        static void Monitor()
        {
            try
            {
                var eventLog = new EventLog();
                if (!EventLog.SourceExists("WslMonitor"))
                {
                    EventLog.CreateEventSource("WslMonitor", "WslMonitor");
                }
                eventLog.Source = "WslMonitor";
                eventLog.Log = "WslMonitor";

                eventLog.WriteEntry("Start wsl-monitor ...");
                if (Process.GetProcessesByName("vcxsrv").Length is 0)
                {
                    eventLog.WriteEntry("VCXsrv is not running");
                    var output = GetProcessOutput("--list --running");
               
                    if (!(output.Contains("No hay distribuciones") || output.Contains("There are no running distributions")))
                    {
                        eventLog.WriteEntry("Wsl is running, killing it");
                        output = GetProcessOutput("--shutdown");
                        eventLog.WriteEntry($"Result: {output}");
                    }
                    else 
                    {
                        eventLog.WriteEntry("Wsl is not running");
                    }
                }
                else 
                {
                    eventLog.WriteEntry("VCXsrv is running");
                }
            }
            catch
            {
                Console.WriteLine("Exception");
            }
        }
    }
}
#!/bin/bash

ERROR_FILE=/mnt/c/users/$(wslvar USERNAME)/.ubuntu/wsl_error.log
OUTPUT_FILE=/mnt/c/users/$(wslvar USERNAME)/.ubuntu/wsl_out.log

echo "-------------------  $1  -------------------" >> $ERROR_FILE
echo "-------------------  $1  -------------------" >> $OUTPUT_FILE

salgo(){
    cat $ERROR_FILE 1>&2
    exit -1
}

disable_services(){
  cmd="genie -i"
  eval "${cmd}" &
  p=$!;
  sleep 30;
  kill $p;

  echo "systemd-modules-load ..."
  genie -c systemctl disable systemd-modules-load

  echo "ssh ..."
  genie -c systemctl disable ssh

  echo "multipathd ..."
  genie -c systemctl disable multipathd

  echo "auditd ..."
  genie -c systemctl disable auditd

  echo "openvpn ..."
  genie -c systemctl disable openvpn

  echo "ufw ..."
  genie -c systemctl disable ufw
  
  echo "lightdm ..."
  genie -c systemctl disable lightdm.service
  
  echo "gpu-manager ..."
  genie -c systemctl disable gpu-manager.service
}

fix_binfmt_misc(){
  echo "" > /etc/fstab
}

if [[ "$1" == "set_username" ]]
then
  sed -i 's/USER_WIN/'"$(wslvar USERNAME)"'/g' /mnt/c/users/$(wslvar USERNAME)/.ubuntu/03_start_desktop.vbs >> $OUTPUT_FILE 2>$ERROR_FILE
fi


########
#   1
########
if [[ "$1" == "update_pkgs" ]]
then
  /usr/bin/curl -s "http://apt.ceibal.edu.uy/wsl2/apt-ceibal.gpg.key" | apt-key add - >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
  
  cat << EOF > /etc/apt/sources.list.d/ceibal.list
deb http://apt.ceibal.edu.uy/ubuntu/ ub20 main
deb-src http://apt.ceibal.edu.uy/ubuntu/ ub20 main
EOF
  
  export DEBIAN_FRONTEND=noninteractive
  dpkg --configure -a >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
  apt --fix-broken -y install >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
  apt-get update >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
  apt upgrade -y >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
  cat "$OUTPUT_FILE" 
fi


########
#   2
########
if [[ "$1" == "install_utils" ]]
then
  export DEBIAN_FRONTEND=noninteractive  
  dpkg --configure -a >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
  apt --fix-broken -y install >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
  apt -y install ceibal-repo-update-wsl2 unzip apt-transport-https libpulse0 wget >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
  cat "$OUTPUT_FILE" 
fi


########
#   3
########
if [[ "$1" == "sound" ]]
then
  username=$(wslvar username)
  
  rm -rf "/mnt/c/users/${username}/downloads/pulse*" >> $OUTPUT_FILE 2>&1
  rm -rf "/mnt/c/users/${username}/downloads/nssm*" >> $OUTPUT_FILE 2>&1
  
  # download pulseaudio to the downloads directory
  wget -O "/mnt/c/users/${username}/downloads/pulseaudio.zip" https://code.x2go.org/releases/binary-win32/3rd-party/pulse/pulseaudio-5.0-rev18.zip >> $OUTPUT_FILE 2>$ERROR_FILE || salgo

  # unzip pulseaudio in the downloads directory
  unzip "/mnt/c/users/${username}/downloads/pulseaudio.zip" -d "/mnt/c/users/${username}/downloads/pulse/" >> $OUTPUT_FILE 2>$ERROR_FILE || salgo

  # move the pulse directory to the root directory of the hard drive
  mv "/mnt/c/users/${username}/downloads/pulse/pulse/" /mnt/c/pulse/ >> $OUTPUT_FILE 2>$ERROR_FILE || salgo

  # add the environment variables to the bash configuration file
  echo -e "\nexport HOST_IP=\"\$(ip route |awk '/^default/{print \$3}')\"\nexport PULSE_SERVER=\"tcp:\$HOST_IP\"\nexport DISPLAY=\"\$(cat /etc/resolv.conf | grep nameserver | awk '{ print \$2 }'):0.0\"\n"

  # reload the bash configuration file
  source ~/.bashrc

  # add the authorized ip address to the pulse configuration file
  echo -e 'load-module module-native-protocol-tcp auth-ip-acl=0.0.0.0/0\nload-module module-esound-protocol-tcp auth-ip-acl=0.0.0.0/0\nload-module module-waveout sink_name=output source_name=input record=0\n' >> /mnt/c/pulse/config.pa

  # disable the automatic shutdown setting
  sed "s/; exit-idle-time = 20/; exit-idle-time = -1/" -i "/mnt/c/pulse/default config files/daemon.conf"

  # download the nssm service manager
  wget -O "/mnt/c/users/${username}/downloads/nssm.zip" https://apt.ceibal.edu.uy/wsl2/nssm-2.24.zip >> $OUTPUT_FILE 2>$ERROR_FILE || salgo

  # unzip the nssm service manager in the downloads directory
  unzip "/mnt/c/users/${username}/downloads/nssm.zip" -d "/mnt/c/users/${username}/downloads/" >> $OUTPUT_FILE 2>$ERROR_FILE || salgo

  # move the nssm service manager to the pulse directory
  mv "/mnt/c/users/${username}/downloads/nssm-2.24/win64/nssm.exe" /mnt/c/pulse/ >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
  
  cat "$OUTPUT_FILE" 
fi


########
#   4
########
if [[ "$1" == "install_genie" ]]
then
	# download the microsoft public key
	apt-key adv --fetch-keys https://packages.microsoft.com/keys/microsoft.asc >> $OUTPUT_FILE 2>$ERROR_FILE || salgo

	# add microsoft to the source list directory for ubuntu 20.04
	sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/ubuntu/20.04/prod focal main" > /etc/apt/sources.list.d/microsoft-prod.list'

	wget -O /etc/apt/trusted.gpg.d/wsl-transdebian.gpg https://arkane-systems.github.io/wsl-transdebian/apt/wsl-transdebian.gpg >> $OUTPUT_FILE 2>$ERROR_FILE || salgo

	# grant permissions to the gpg key
	chmod a+r /etc/apt/trusted.gpg.d/wsl-transdebian.gpg >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
	# add arkane systems to the source list directory

cat << EOF > /etc/apt/sources.list.d/wsl-transdebian.list
deb https://arkane-systems.github.io/wsl-transdebian/apt/ $(lsb_release -cs) main
deb-src https://arkane-systems.github.io/wsl-transdebian/apt/ $(lsb_release -cs) main
EOF

	# update the linux package information
	apt update >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
	# install genie
	apt -y install systemd-genie >> $OUTPUT_FILE 2>$ERROR_FILE || salgo

    # Salgo de la botella en shutdown o reboot.
cat << EOF > /etc/init.d/K99_genie
/usr/bin/genie -u 
EOF
    chmod a+x /etc/init.d/K99_genie
    ln -nsf /etc/init.d/K99_genie /etc/rc0.d/K99_genie
    ln -nsf /etc/init.d/K99_genie /etc/rc6.d/K99_genie
	
    cat "$OUTPUT_FILE"
fi


########
#   5
########
if [[ "$1" == "install_gui" ]]
then
  export DEBIAN_FRONTEND=noninteractive 
  dpkg --configure -a >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
  apt --fix-broken -y install >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
  apt -y install --no-install-recommends lxde language-selector-gnome gdebi gnome-software ceibal-configure-wsl2-desktop google-chrome-stable language-pack-es wspanish >> $OUTPUT_FILE 2>$ERROR_FILE || salgo 

  #Associa los .deb a gdebi
  xdg-mime default gdebi.desktop application/vnd.debian.binary-package >> $OUTPUT_FILE 2>$ERROR_FILE || salgo
  
  cat "$OUTPUT_FILE"
fi


########
#   6
########
if [[ "$1" == "user_ceibal" ]]
then
  useradd -m -s /bin/bash ceibal >> $OUTPUT_FILE 2>$ERROR_FILE
  echo 'ceibal:ceibal' | chpasswd; usermod -aG sudo ceibal >> $OUTPUT_FILE 2>$ERROR_FILE 
  echo 'ceibal ALL=(ALL) NOPASSWD:ALL' |  EDITOR='tee' visudo --file /etc/sudoers.d/ceibal >> $OUTPUT_FILE 2>$ERROR_FILE || salgo

  cat << EOF > /etc/default/locale
LANG=es_UY.UTF-8
LANGUAGE=es_UY:es
EOF

  cat << EOF > /home/ceibal/.pam_environment
LANG=es_UY
LANGUAGE=es_UY
EOF

[ ! -d "/home/ceibal/.config/" ] && mkdir /home/ceibal/.config


  cat << EOF > /home/ceibal/.config/mimeapps.list
[Default Applications]
text/html=google-chrome.desktop
x-scheme-handler/http=google-chrome.desktop
x-scheme-handler/https=google-chrome.desktop
x-scheme-handler/about=google-chrome.desktop
x-scheme-handler/unknown=google-chrome.desktop
application/vnd.debian.binary-package=gdebi.desktop

[Added Associations]
EOF
  
  chown -R ceibal:ceibal /home/ceibal/
  
  cat "$OUTPUT_FILE"
fi

########
#   7
########
if [[ "$1" == "services" ]]
then
  disable_services
  fix_binfmt_misc
  cat "$OUTPUT_FILE"
fi



@ECHO OFF & NET SESSION >NUL 2>&1
IF %ERRORLEVEL% == 0 (ECHO Permisos de Administrador check OK ...) ELSE (ECHO Debe ejecutar este script con permisos de Administrador && pause && goto ENDSCRIPT)


SET INSTALADOR_FOLDER="%LOCALAPPDATA%\Instalador Linux WSL2"
SET APPX_NAME="CanonicalGroupLimited.Ubuntu20.04onWindows"



ECHO 1) Eliminando carpeta y archivos del instalador
PowerShell.exe -ExecutionPolicy bypass -command "Remove-Item -Path '%INSTALADOR_FOLDER%' -Force -Recurse *>&1 | out-null"
PowerShell.exe -ExecutionPolicy bypass -command "Remove-Item -Path '%TEMP%' -Force -Recurse *>&1 | out-null"

ECHO 2) Desinstalando Ubuntu
PowerShell.exe -ExecutionPolicy bypass -command "Get-AppxPackage *ubuntu* | Remove-AppxPackage -AllUsers *>&1| out-null"

ECHO 3) Desinstalando Xserver
PowerShell.exe -ExecutionPolicy bypass -command "choco uninstall vcxsrv -y *>&1 | out-null"

ECHO 4) Deteniendo servicio de sonido
PowerShell.exe -ExecutionPolicy bypass -command "net stop pulseaudio"

ECHO 5) Elimininando servicio de sonido
PowerShell.exe -ExecutionPolicy bypass -command "C:\\pulse\\nssm.exe remove pulseaudio"

ECHO 6) Elimininando archivos de sonido
PowerShell.exe -ExecutionPolicy bypass -command "Remove-Item -Path 'c:\\pulse' -Force -Recurse *>&1 | out-null"
PowerShell.exe -ExecutionPolicy bypass -command "Remove-Item -Path '%USERPROFILE%\Downloads\pulse*' -Force -Recurse *>&1 | out-null"
PowerShell.exe -ExecutionPolicy bypass -command "Remove-Item -Path '%USERPROFILE%\Downloads\nssm*' -Force -Recurse *>&1 | out-null"


ECHO Finalizo la desinstalacion!
ECHO ""
ECHO ********** Se reiniciara el sistema ***********

PowerShell.exe -ExecutionPolicy bypass -command "Start-Sleep -Seconds 3"
PowerShell.exe -ExecutionPolicy bypass -command "Restart-Computer"

' define variables
dim application_object, shell_object, userprofile, command_object, standard_output_string

' create application object
set application_object = createobject("shell.application")

' create shell object
set shell_object = createobject("wscript.shell")

' store environment variable
username = shell_object.expandenvironmentstrings("%username%")

' run powershell in the background
application_object.shellexecute "powershell", "-file c:\users\" & username & "\.ubuntu\01_reload_vcxsrv.ps1", "", "", 0

' wait for powershell script to complete
wscript.sleep 3000

' create command object
set command_object = shell_object.exec("wsl genie --is-in-bottle")

' store standard output
standard_output = command_object.stdout.readall

' execute contents if shell is inside the genie container
if instr(standard_output, "inside") > 0 then
  
  ' run bash script
  shell_object.run "bash /mnt/c/users/" & username & "/.ubuntu/02_start_desktop.sh", 0

' execute contents if shell is outside the genie container   
else 
  
  ' run bash script
 shell_object.run "wsl genie -c bash /mnt/c/users/" & username & "/.ubuntu/02_start_desktop.sh", 0

end if
